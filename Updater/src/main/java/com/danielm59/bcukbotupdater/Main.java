package com.danielm59.bcukbotupdater;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class Main
{
	private static final Logger logger = LoggerFactory.getLogger("BCUK_BOT_UPDATER");

	public static void main(String[] args)
	{
		try
		{
			getFile();
			upzip();
			if (SystemUtils.IS_OS_WINDOWS)
			{
				Runtime.getRuntime().exec(new String[]{"cmd.exe", "/c", "start java -jar BCUK_BOT.jar"});
			} else if (SystemUtils.IS_OS_LINUX)
			{
				Runtime.getRuntime().exec(new String[]{"xfce4-terminal", "-e", "java -jar BCUK_BOT.jar"});
			}
		} catch (IOException e)
		{
			logger.error(e.toString());
		}
	}

	private static void getFile() throws IOException
	{
		logger.info("Downloading file");
		URL url = new URL("https://gitlab.com/api/v4/projects/11679854/jobs/artifacts/master/download?job=build");
		Path targetPath = new File("update.zip").toPath();
		Files.copy(url.openStream(), targetPath, StandardCopyOption.REPLACE_EXISTING);
		logger.info("Download complete");
	}

	private static void upzip() throws IOException
	{
		logger.info("Unzipping file");
		try (ZipFile zipFile = new ZipFile(new File("update.zip")))
		{
			Enumeration<? extends ZipEntry> entries = zipFile.entries();
			while (entries.hasMoreElements())
			{
				ZipEntry entry = entries.nextElement();
				if (!entry.isDirectory() && !entry.getName().contains("Updater.jar"))
				{
					Path path = Paths.get(entry.getName());
					File entryDestination = new File(path.getFileName().toString());
					InputStream in = zipFile.getInputStream(entry);
					OutputStream out = new FileOutputStream(entryDestination);
					IOUtils.copy(in, out);
					IOUtils.closeQuietly(in);
					out.close();
				}
			}
		}
		logger.info("Unzip complete");
	}
}
