package com.danielm59.bcukbot;

import com.danielm59.botlib.commands.Counters;
import com.danielm59.botlib.configs.Cache;
import com.danielm59.botlib.configs.Config;
import com.danielm59.botlib.configs.ConfigHandler;
import com.danielm59.botlib.configs.MusicConfig;
import com.danielm59.botlib.configs.SFXfiles;
import com.danielm59.botlib.discord.DiscordBot;
import com.danielm59.botlib.discord.pointSystem.PointSystem;
import com.danielm59.botlib.games.trivia.api.TriviaAPI;
import com.danielm59.botlib.twitch.OnlineTracker;
import com.danielm59.botlib.twitch.irc.IRCBot;
import com.danielm59.botlib.twitch.irc.IRCListener;
import discord4j.core.event.domain.message.MessageCreateEvent;
import reactor.core.publisher.Mono;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main
{
    private static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    public static final DiscordBot discordBot = new DiscordBot();
    
    public static Config config = new Config();
    public static Cache cache = new Cache();
    public static SFXfiles sfxFiles = new SFXfiles();
    public static MusicConfig musicConfig = new MusicConfig();
    public static IRCBot twitchBot;
    public static Counters counters = new Counters();

    public static void main(String[] args)
    {
        TriviaAPI.getSessionToken(); //Setup trivia API
        config = ConfigHandler.loadJson(config);
        cache = ConfigHandler.loadJson(cache);
        sfxFiles = ConfigHandler.loadJson(sfxFiles);
        musicConfig = ConfigHandler.loadJson(musicConfig);
        counters = ConfigHandler.loadJson(counters);
        PointSystem.init();
        twitchBot = new IRCBot();
        scheduler.scheduleAtFixedRate(OnlineTracker::checkStatus, 60, 15, TimeUnit.SECONDS);
        //scheduler.scheduleAtFixedRate(JustGivingAPI::updateData, 0, 120, TimeUnit.SECONDS);

        discordBot.registerModule(new CustomDiscordCommands());
        discordBot.registerModule(counters);
        discordBot.init(); //This has to be done last as it is blocking
    }

    static Mono<Void> reloadConfig(MessageCreateEvent event)
    {
        ConfigHandler.loadJson(config);
        ConfigHandler.loadJson(sfxFiles);
        ConfigHandler.loadJson(counters);
        discordBot.registerModule(counters);
        Counters.setupTwitchCommands(IRCListener.commands);
        return event.getMessage().getChannel().flatMap(channel -> channel.createMessage("Configs Reloaded")).then();
    }
}
