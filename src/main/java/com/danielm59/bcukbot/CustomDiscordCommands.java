package com.danielm59.bcukbot;

import com.danielm59.botlib.commands.discord.Command;
import com.danielm59.botlib.commands.discord.CommandCategory;
import com.danielm59.botlib.commands.discord.DiscordCommandRegistry;
import com.danielm59.botlib.commands.discord.DiscordCommands;
import com.danielm59.botlib.commands.discord.Permissions;
import discord4j.core.event.domain.message.MessageCreateEvent;
import reactor.core.publisher.Mono;

public class CustomDiscordCommands implements DiscordCommandRegistry
{
    @Override
    public void registerCommands(DiscordCommands commands)
    {
        commands.registerCommand(CommandCategory.GENERAL, "#ShoesForWillow", new Command(CustomDiscordCommands::shoes, Permissions::general));
        commands.registerCommand(CommandCategory.ADMIN, "!reload", new Command(Main::reloadConfig, Permissions::admin));
    }

    private static Mono<Void> shoes(MessageCreateEvent event)
    {
        return event.getMessage().getChannel().flatMap(
                channel -> channel.createMessage(":high_heel: :sandal: :boot: :mans_shoe: :athletic_shoe:")).then();
    }
}
